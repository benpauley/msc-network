from pathlib import Path
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import Xception, VGG16, InceptionResNetV2
from keras.layers import Input, GlobalAveragePooling2D, Dense, Dropout
from keras.models import Model, Sequential
from keras.optimizers import RMSprop, Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
from math import ceil
import os
import argparse
import shutil
import pandas as pd
import numpy as np
from tqdm import tqdm
from keras.models import load_model
import tensorflow as tf
import math

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--pathology", required=True)
ap.add_argument("-n", "--n_images", required=True)
ap.add_argument("-v", "--n_val", default=8)
ap.add_argument("-m", "--model", default="Xception")
args = vars(ap.parse_args())

pathology = args['pathology']
n = int(args['n_images'])
n_val = int(args['n_val'])
model_name = args['model']

print("Using the {} model".format(model_name))

def create_split(pathology, n, n_val, test=False):
    image_dir = '/mnt/data1/ben/images/development'
    split_dir = '/mnt/data1/ben/data'

    # Delete split_dir, any subdirectories and files
    try:
        shutil.rmtree(split_dir)
    except FileNotFoundError:
        pass

    os.mkdir(split_dir)

    # Recreate the directories
    for a in ['train', 'validation']:
        for b in ['pathology', 'not pathology']:
            os.makedirs(r'{}/{}/{}'.format(split_dir, a, b))

    valid_images = os.listdir(image_dir)
    data = pd.read_csv('/mnt/data1/ben/data_files/encoded_pathologies.csv')
    df = data[data['Image Index'].isin(valid_images)]

    positive_class = df[df['Finding Labels'] == pathology]
    negative_class = df[df['Finding Labels'] == 'No Finding']

    positive_class = positive_class['Image Index'].tolist()
    negative_class = negative_class['Image Index'].tolist()

    if len(positive_class) < n:
        raise ValueError("Not enough samples")

    positive_train = np.random.choice(positive_class, n-n_val, replace=False)
    remainder = np.setdiff1d(positive_class, positive_train)
    positive_validation = np.random.choice(remainder, n_val, replace=False)

    negative_train = np.random.choice(negative_class, n-n_val, replace=False)
    remainder = np.setdiff1d(negative_class, negative_train)
    negative_validation = np.random.choice(remainder, n_val, replace=False)

    print("Generating positive train images ...")
    for image in tqdm(positive_train):
        shutil.copyfile(r'{}/{}'.format(image_dir, image), r'{}/train/pathology/{}'.format(split_dir, image))

    print("Generating positive validation images ...")
    for image in tqdm(positive_validation):
        shutil.copyfile(r'{}/{}'.format(image_dir, image), r'{}/validation/pathology/{}'.format(split_dir, image))

    print("Generating negative train images ...")
    for image in tqdm(negative_train):
        shutil.copyfile(r'{}/{}'.format(image_dir, image), r'{}/train/not pathology/{}'.format(split_dir, image))

    print("Generating negative validation images ...")
    for image in tqdm(negative_validation):
        shutil.copyfile(r'{}/{}'.format(image_dir, image), r'{}/validation/not pathology/{}'.format(split_dir, image))

    if test:
        print("Generating test images ...")
        test_dir = Path('/mnt/data1/ben/images/test')
        for x in ['pathology', 'not pathology']:
            os.makedirs(r'{}/test/{}'.format(split_dir, x))
        valid_images = os.listdir(test_dir)
        test_df = data[data['Image Index'].isin(valid_images)]
        positive_test = test_df[test_df['Finding Labels'] == pathology]
        negative_test = test_df[test_df['Finding Labels'] == 'No Finding'].sample(positive_test.shape[0])
        positive_test = positive_test['Image Index'].tolist()
        negative_test = negative_test['Image Index'].tolist()

        for image in tqdm(positive_test):
            shutil.copyfile('{}/{}'.format(test_dir, image), '{}/test/pathology/{}'.format(split_dir, image))

        for image in tqdm(negative_test):
            shutil.copyfile('{}/{}'.format(test_dir, image), '{}/test/not pathology/{}'.format(split_dir, image))

def make_dirs(model_name):
    """Make the model directories if they don't exist"""
    root = '/mnt/data1/ben/models/proper_runs/'
    if not os.path.isdir('{}/{}'.format(root, model_name)):
        os.mkdir('{}/{}'.format(root, model_name))
    
    if not os.path.isdir('{}/{}/pretrained'.format(root, model_name)):
        os.mkdir('{}/{}/pretrained'.format(root, model_name))

    if not os.path.isdir('{}/{}/pretrained/training_results'.format(root, model_name)):
        os.makedirs('{}/{}/pretrained/training_results'.format(root, model_name))
    
    if not os.path.isdir('{}/{}/pretrained/checkpoints'.format(root, model_name)):
        os.makedirs('{}/{}/pretrained/checkpoints'.format(root, model_name))

def save_bottleneck_features():
    datagen = ImageDataGenerator(rescale=1. / 255)

    # build the VGG16 network
    model = models[model_name](include_top=False, weights='imagenet')

    generator = datagen.flow_from_directory(
        train_dir,
        target_size=(shape_size, shape_size),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)
    
    size = int(math.ceil(((n-n_val)*2)/batch_size))
    bottleneck_features_train = model.predict_generator(
        generator, size)
    np.save('/mnt/data1/ben/models/proper_runs/{}/pretrained/bottleneck_features_train.npy'.format(model_name),
                bottleneck_features_train)

    generator = datagen.flow_from_directory(
        validation_dir,
        target_size=(shape_size, shape_size),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)
    
    size = int(math.ceil((n_val*2)/batch_size))
    bottleneck_features_validation = model.predict_generator(
        generator, size)
    np.save('/mnt/data1/ben/models/proper_runs/{}/pretrained/bottleneck_features_validation.npy'.format(model_name),
            bottleneck_features_validation)
    
    n_test = len(os.listdir('{}/pathology'.format(test_dir)))
    generator = datagen.flow_from_directory(
        test_dir,
        target_size=(shape_size, shape_size),
        class_mode=None,
        shuffle=False)
    
    bottleneck_features_test = model.predict_generator(
        generator)
    np.save('/mnt/data1/ben/models/proper_runs/{}/pretrained/bottleneck_features_test.npy'.format(model_name),
            bottleneck_features_test)

def train_top_model():
    train_data = np.load('/mnt/data1/ben/models/proper_runs/{}/pretrained/bottleneck_features_train.npy'.format(model_name))
    train_labels = np.array(
        [0, 1] * (n-n_val) + [1, 0] * (n-n_val)).reshape((n-n_val)*2, 2)

    print(train_data.shape)

    validation_data = np.load('/mnt/data1/ben/models/proper_runs/{}/pretrained/bottleneck_features_validation.npy'.format(model_name))
    validation_labels = np.array(
        [0, 1] * (n_val) + [1, 0] * (n_val)).reshape((n_val)*2, 2)

    model = Sequential()
    model.add(GlobalAveragePooling2D())
    model.add(Dense(units=1024, activation='relu'))
    model.add(Dropout(0.3))
    model.add(Dense(units=2, activation='softmax'))

    opt = Adam(lr=1e-5)

    model.compile(optimizer=opt,
                  loss='binary_crossentropy', metrics=['accuracy'])
    
    es = EarlyStopping(patience=5, monitor='val_loss')
    chkpt1 = ModelCheckpoint(filepath='/mnt/data1/ben/models/proper_runs/{}/pretrained/checkpoints/{}_highest_val_acc'.format(model_name, pathology), monitor='val_acc', mode='max',
                                save_best_only=True, save_weights_only=True)
    chkpt2 = ModelCheckpoint(filepath='/mnt/data1/ben/models/proper_runs/{}/pretrained/checkpoints/{}_lowest_val_loss'.format(model_name, pathology), monitor='val_loss', mode='min',
                                save_best_only=True, save_weights_only=True)

    history = model.fit(train_data, train_labels,
                    epochs=epochs, batch_size=batch_size,
                    validation_data=(validation_data, validation_labels),
                    callbacks=[es, chkpt1, chkpt2])
    
    results = pd.DataFrame({'Training accuracy': history.history['acc'],
                        'Training loss': history.history['loss'],
                        'Validation accuracy': history.history['val_acc'],
                        'Validation loss': history.history['val_loss']})

    results.to_csv('/mnt/data1/ben/models/proper_runs/{}/pretrained/training_results/{}_results.csv'.format(model_name, pathology))

    print("Testing model ...")
    model.load_weights('/mnt/data1/ben/models/proper_runs/{}/pretrained/checkpoints/{}_highest_val_acc'.format(model_name, pathology))

    n_test = len(os.listdir('{}/pathology'.format(test_dir)))
    test_data = np.load('/mnt/data1/ben/models/proper_runs/{}/pretrained/bottleneck_features_test.npy'.format(model_name, pathology))
    test_labels = np.array(
        [0, 1] * (n_test) + [1, 0] * (n_test)).reshape((n_test)*2, 2)
    
    preds = model.predict(test_data, batch_size=1)
    print(preds)

    l1 = test_labels[:, 0]
    l2 = test_labels[:, 1]
    p1 = preds[:, 0]
    p2 = preds[:, 1]
    pos_images = os.listdir(test_dir / 'pathology')
    neg_images = os.listdir(test_dir / 'not pathology')
    images = pos_images + neg_images

    output = pd.DataFrame({'Image': images, 'Label1': l1, 'Label2': l2, 'P(pathology)': p1, 'P(!pathology)': p2})
    output.to_csv('/mnt/data1/ben/models/proper_runs/{}/pretrained/{}_results.csv'.format(model_name, pathology), index=False)

### START OF SCRIPT ###
models = {'Xception': Xception, 'VGG16': VGG16}

print("Training the model for {} ...".format(pathology))

create_split(pathology, n, n_val, test=True)
make_dirs(model_name)

data_dir = Path('/mnt/data1/ben/data')
train_dir = data_dir / 'train'
validation_dir = data_dir / 'validation'

batch_size = 32
shape_size = 224
epochs = 5000

test_dir = Path('/mnt/data1/ben/data/test')

save_bottleneck_features()
train_top_model()
