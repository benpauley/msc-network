from pathlib import Path
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import Xception, VGG16, InceptionResNetV2
from keras.layers import Input, GlobalAveragePooling2D, Dense, Dropout
from keras.models import Model
from keras.optimizers import RMSprop, Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
from math import ceil
import os
import argparse
import shutil
import pandas as pd
import numpy as np
from tqdm import tqdm
from keras.models import load_model
import tensorflow as tf

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--pathology", required=True)
ap.add_argument("-n", "--n_images", required=True)
ap.add_argument("-v", "--n_val", default=8)
ap.add_argument("-m", "--model", default="Xception")
args = vars(ap.parse_args())

pathology = args['pathology']
n = int(args['n_images'])
n_val = int(args['n_val'])
model_name = args['model']

print("Using the {} model".format(model_name))

def create_split(pathology, n, n_val, test=False):
    image_dir = '/mnt/data1/ben/images/development'
    split_dir = '/mnt/data1/ben/data'

    # Delete split_dir, any subdirectories and files
    try:
        shutil.rmtree(split_dir)
    except FileNotFoundError:
        pass

    os.mkdir(split_dir)

    # Recreate the directories
    for a in ['train', 'validation']:
        for b in ['pathology', 'not pathology']:
            os.makedirs(r'{}/{}/{}'.format(split_dir, a, b))

    valid_images = os.listdir(image_dir)
    data = pd.read_csv('/mnt/data1/ben/data_files/encoded_pathologies.csv')
    df = data[data['Image Index'].isin(valid_images)]

    positive_class = df[df['Finding Labels'] == pathology]
    negative_class = df[df['Finding Labels'] == 'No Finding']

    positive_class = positive_class['Image Index'].tolist()
    negative_class = negative_class['Image Index'].tolist()

    if len(positive_class) < n:
        raise ValueError("Not enough samples")

    positive_train = np.random.choice(positive_class, n-n_val, replace=False)
    remainder = np.setdiff1d(positive_class, positive_train)
    positive_validation = np.random.choice(remainder, n_val, replace=False)

    negative_train = np.random.choice(negative_class, n-n_val, replace=False)
    remainder = np.setdiff1d(negative_class, negative_train)
    negative_validation = np.random.choice(remainder, n_val, replace=False)

    print("Generating positive train images ...")
    for image in tqdm(positive_train):
        shutil.copyfile(r'{}/{}'.format(image_dir, image), r'{}/train/pathology/{}'.format(split_dir, image))

    print("Generating positive validation images ...")
    for image in tqdm(positive_validation):
        shutil.copyfile(r'{}/{}'.format(image_dir, image), r'{}/validation/pathology/{}'.format(split_dir, image))

    print("Generating negative train images ...")
    for image in tqdm(negative_train):
        shutil.copyfile(r'{}/{}'.format(image_dir, image), r'{}/train/not pathology/{}'.format(split_dir, image))

    print("Generating negative validation images ...")
    for image in tqdm(negative_validation):
        shutil.copyfile(r'{}/{}'.format(image_dir, image), r'{}/validation/not pathology/{}'.format(split_dir, image))

    if test:
        print("Generating test images ...")
        test_dir = Path('/mnt/data1/ben/images/test')
        for x in ['pathology', 'not pathology']:
            os.makedirs(r'{}/test/{}'.format(split_dir, x))
        valid_images = os.listdir(test_dir)
        test_df = data[data['Image Index'].isin(valid_images)]
        positive_test = test_df[test_df['Finding Labels'] == pathology]
        negative_test = test_df[test_df['Finding Labels'] == 'No Finding'].sample(positive_test.shape[0])
        positive_test = positive_test['Image Index'].tolist()
        negative_test = negative_test['Image Index'].tolist()

        for image in tqdm(positive_test):
            shutil.copyfile('{}/{}'.format(test_dir, image), '{}/test/pathology/{}'.format(split_dir, image))

        for image in tqdm(negative_test):
            shutil.copyfile('{}/{}'.format(test_dir, image), '{}/test/not pathology/{}'.format(split_dir, image))

def make_dirs(model_name):
    """Make the model directories if they don't exist"""   
    root = '/mnt/data1/ben/models/proper_runs/'
    if not os.path.isdir('{}/{}'.format(root, model_name)):
        os.mkdir('{}/{}'.format(root, model_name))
    
    if not os.path.isdir('{}/{}/partial'.format(root, model_name)):
        os.mkdir('{}/{}/partial'.format(root, model_name))
    
    if not os.path.isdir('{}/{}/partial/training_results'.format(root, model_name)):
        os.mkdir('{}/{}/partial/training_results'.format(root, model_name))
    
    if not os.path.isdir('{}/{}/partial/checkpoints'.format(root, model_name)):
        os.mkdir('{}/{}/partial/checkpoints'.format(root, model_name))


print("Training the model for {} ...".format(pathology))

create_split(pathology, n, n_val, test=True)
make_dirs(model_name)

data_dir = Path('/mnt/data1/ben/data')
train_dir = data_dir / 'train'
validation_dir = data_dir / 'validation'   

batch_size = 32
shape_size = 224
epochs = 100

train_datagen = ImageDataGenerator(rescale=1/255., rotation_range=5,
                    width_shift_range=0.1, height_shift_range=0.05,
                    shear_range=0.1, zoom_range=0.15, horizontal_flip=True,
                    vertical_flip=False,fill_mode="reflect")

validation_datagen = ImageDataGenerator(rescale=1/255.)

train_generator = train_datagen.flow_from_directory(train_dir,
                    classes=('pathology', 'not pathology'), target_size=(shape_size, shape_size),
                    batch_size=batch_size, class_mode='categorical', shuffle=True)

validation_generator = validation_datagen.flow_from_directory(validation_dir,
                        classes=('pathology', 'not pathology'), target_size=(shape_size, shape_size),
                        batch_size=batch_size, class_mode='categorical', shuffle=False)

def build_model_xception():
    conv_base = Xception(include_top=False, weights='imagenet', input_shape=(shape_size, shape_size, 3))
    for layer in conv_base.layers[:26]:
        layer.trainable = False

    x = GlobalAveragePooling2D()(conv_base.output)
    x = Dense(units=1024, activation='relu')(x)
    x = Dropout(0.3)(x)
    x = Dense(units=2, activation='softmax')(x)
    model = Model(input=conv_base.input, output=x)

    return model

def build_model_vgg16():
    conv_base = VGG16(include_top=False, weights='imagenet', input_shape=(shape_size, shape_size, 3))
    # Do we want to freeze any weights?
    for layer in conv_base.layers[:7]:
        layer.trainable = False

    x = GlobalAveragePooling2D()(conv_base.output)
    x = Dense(units=1024, activation='relu')(x)
    x = Dropout(0.3)(x)
    x = Dense(units=2, activation='softmax')(x)
    model = Model(input=conv_base.input, output=x)

    return model

models = {'Xception': build_model_xception, 'VGG16': build_model_vgg16}

model = models[model_name]()
opt = Adam(lr=1e-5)
model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])

training_step_size = ceil(len(os.listdir(train_dir / 'pathology')) + len(os.listdir(train_dir / 'not pathology')))//batch_size
validation_step_size = ceil(len(os.listdir(validation_dir / 'pathology')) + len(os.listdir(validation_dir / 'not pathology')))//batch_size

es = EarlyStopping(patience=5, monitor='val_loss')
chkpt1 = ModelCheckpoint(filepath='/mnt/data1/ben/models/proper_runs/{}/partial/checkpoints/{}_highest_val_acc'.format(model_name, pathology), monitor='val_acc', mode='max',
                            save_best_only=True, save_weights_only=True)
chkpt2 = ModelCheckpoint(filepath='/mnt/data1/ben/models/proper_runs/{}/partial/checkpoints/{}_lowest_val_loss'.format(model_name, pathology), monitor='val_loss', mode='min',
                            save_best_only=True, save_weights_only=True)   
history = model.fit_generator(train_generator, steps_per_epoch=training_step_size,
                    epochs=epochs, validation_data=validation_generator,
                    validation_steps=validation_step_size, callbacks=[es, chkpt1, chkpt2])

results = pd.DataFrame({'Training accuracy': history.history['acc'],
                        'Training loss': history.history['loss'],
                        'Validation accuracy': history.history['val_acc'],
                        'Validation loss': history.history['val_loss']})

results.to_csv('/mnt/data1/ben/models/proper_runs/{}/partial/training_results/{}_results.csv'.format(model_name, pathology), index=False)

print("Testing model ...")
test_model = models[model_name]()
test_model.load_weights('/mnt/data1/ben/models/proper_runs/{}/partial/checkpoints/{}_highest_val_acc'.format(model_name, pathology))
test_dir = Path('/mnt/data1/ben/data/test')
test_generator = validation_datagen.flow_from_directory(test_dir,
                    classes=('pathology', 'not pathology'), target_size=(shape_size, shape_size),
                    batch_size=1, class_mode='categorical', shuffle=False)
test_step_size = ceil(len(os.listdir(test_dir / 'pathology')) + len(os.listdir(test_dir / 'not pathology')))
preds = test_model.predict_generator(test_generator, steps=test_step_size)
pos_images = os.listdir(test_dir / 'pathology')
pos_labels = [1 for i in pos_images]
neg_images = os.listdir(test_dir / 'not pathology')
neg_labels = [0 for i in neg_images]
images = pos_images + neg_images
labels = pos_labels + neg_labels
output = pd.DataFrame(preds, columns=['P(pathology)', 'P(!pathology)'])
output['Image'] = images
output['Label'] = labels
output = output[['Image', 'Label', 'P(pathology)', 'P(!pathology)']]
output.to_csv('/mnt/data1/ben/models/proper_runs/{}/partial/{}_results.csv'.format(model_name, pathology), index=False)

